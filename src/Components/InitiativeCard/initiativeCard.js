import React, { Component } from 'react';
import './styles.css';
import { LIST_INITIATIVES_PATH } from '../../Constants/globals';
import cloneDeep from 'lodash/cloneDeep';

class InitiativeCard extends Component {
    state = {
        showButtons: false,
    }
    showComments = (id) => {
        window.location.href = '/comments/' + id;
    }

    voteInfavor = (e) => {
        let initiative = cloneDeep(this.props.data);
        initiative.puolesta_aanet +=1;
        initiative.is_voted = true;
        this.props.vote(initiative);
    }

    voteAgainst = (e) => {
        let initiative = cloneDeep(this.props.data);
        initiative.vastaan_aanet +=1;
        initiative.is_voted = true;
        this.props.vote(initiative);
    }

    voteDontKnow = (e) => {
        let initiative = cloneDeep(this.props.data);
        initiative.hidden = true;
        this.props.vote(initiative);
    }

    show = (e) => {
        let showButtons = cloneDeep(this.state.showButtons);
        this.setState({ showButtons: !showButtons })
    }
    render() {

        let all_votes = this.props.data.puolesta_aanet + this.props.data.vastaan_aanet;
        let upvotes = (this.props.data.puolesta_aanet / all_votes) * 100
        upvotes = upvotes.toFixed(1);
        upvotes = String(upvotes) + '%';
        let downvotes = (this.props.data.vastaan_aanet / all_votes) * 100
        downvotes = downvotes.toFixed(1);
        downvotes = String(downvotes) + '%';

        
        return (
            <div className="initiative-card">
                <div className={this.state.showButtons ? "initiative-information-container-shorter" : "initiative-information-container"}>
                    <div className="initiative-information-smalls">
                        <small className="initiative-information-target">{this.props.data.kategoria}</small>
                        <small className="initiative-information-votes">{this.props.data.puolesta_aanet + this.props.data.vastaan_aanet + ' ääntä'}</small>
                    </div>

                    <div className="initiative-information-content" onClick={e => this.showComments(this.props.data.id)}>
                        <h5 className="initiative-name">{this.props.data.otsikko}</h5>
                        <p className="initiative-description">{this.props.data.kuvaus}</p>
                    </div>
                </div>

                {this.state.showButtons ?
                    <div className="initiative-actions-container-wider">
                        <button className="btn btn-link no-active" onClick={this.show}><i className="fa fa-ellipsis-h" style={{ paddingLeft: '2px' }}></i> Pienennä</button>
                        <button className="btn btn-link no-active"><i className="fa fa-trash" style={{ paddingLeft: '2px' }}></i> Roskaposti</button>
                        <button className="btn btn-link no-active"><i className="fa fa-comments" style={{ paddingLeft: '2px' }}></i> Kommentoi</button>
                    </div>
                    :
                    <div className="initiative-actions-container" onClick={this.show}>
                        <button className="btn btn-link"><i className="fa fa-ellipsis-h" style={{ paddingLeft: '2px' }}></i></button>
                    </div>
                }
                {this.props.data.is_voted ?
                    <div className="initiative-result-container" style={{padding: '15px', borderTop: 'solid 1px #71e5fe'}}>
                        <small>Tulokset</small>
                        <div className="progress">
                        <div className="progress-bar bg-success" role="progressbar" style={{width: upvotes, padding: '5px'}}>{upvotes}</div>
                            <div className="progress-bar bg-danger" role="progressbar" style={{width: downvotes, padding: '5px'}}>{downvotes}</div>
                        </div>
                    </div>
                    :
                    <div className="initiative-vote-buttons-container">
                        <button className="btn btn-link" style={{ color: 'forestgreen' }} onClick={this.voteInfavor}><i className="fa fa-thumbs-up"></i> <span>Puolesta</span></button>
                        <button className="btn btn-link" style={{ color: 'firebrick' }} onClick={this.voteAgainst}><i className="fa fa-thumbs-down"></i> <span>Vastaan</span></button>
                        <button className="btn btn-link" style={{ color: '#008B8B', borderRadius: '0px', float: 'right' }} onClick={this.voteDontKnow}>Ei koske minua</button>
                    </div>
                }


            </div>
        );
    }
}

export default InitiativeCard;