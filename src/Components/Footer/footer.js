import React, { Component } from 'react';
import './footer.css';
import { LIST_CHANNELS_PATH, CREATE_INITIATIVE_PATH, LIST_INITIATIVES_PATH } from '../../Constants/globals';

class Footer extends Component {
    redirectToChannels = (e) => {
        window.location.href = LIST_CHANNELS_PATH;
    }

    redirectToCreateInitiative = (e) => {
        window.location.href = CREATE_INITIATIVE_PATH;
    }

    redirectToMainFeed = (e) => {
        window.location.href = LIST_INITIATIVES_PATH;
    }
    render() {
        return (
            <div className="footer-container">
                <div className="mainfeed-container button-container">
                    <button className="footer-button mainfeed" onClick={this.redirectToMainFeed}>Pääsyöte</button>
                </div>
                <div className="create-container button-container">
                    <button className="footer-button create text-center" onClick={this.redirectToCreateInitiative}>Luo aloite</button>
                </div>

                <div className="channels-container button-container">
                    <button className="footer-button channels" onClick={this.redirectToChannels}>Yhteisöt</button>
                </div>
            </div>
        );
    }
}

export default Footer;