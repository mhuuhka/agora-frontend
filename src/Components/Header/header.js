import React, { Component } from 'react';
import './header.css';

class Header extends Component {
    render() {
        return (
            <div className="header-container">
                <h2 className="header-heading">Agora</h2>
            </div>
        );
    }
}

export default Header;