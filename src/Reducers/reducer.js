import { loop, Cmd } from 'redux-loop';
import cloneDeep from 'lodash/cloneDeep';
import { LIST_INITIATIVES_PATH } from '../Constants/globals';

const redirect = () => {
    window.location.href = LIST_INITIATIVES_PATH;
}
const Run = (func, ...args) => Cmd.run(func, {
    successActionCreator: x => x,
    args
});
const initialState = {
    initiatives: [
        {
            kategoria: 'Ting Hackathon',
            otsikko: 'Ensi vuoden hackathon',
            kuvaus: 'Tulisiko Ting Hackathon järjestää ensi vuonna uudelleen?',
            puolesta_aanet: 100,
            vastaan_aanet: 0,
            is_voted: false,
            id: 0,
            comments: [
                { comment: 'Ehdottomasti!', opinion: 'Puolesta'},
                { comment: 'Todellakin!', opinion: 'Puolesta'},
                { comment: 'Kyllä!', opinion: 'Puolesta'},
                { comment: 'Myönteinen vastaus!', opinion: 'Puolesta'},
                { comment: 'Tottakai!', opinion: 'Puolesta'},
                { comment: 'Ehdottomasti!', opinion: 'Puolesta'},
                { comment: 'Todellakin!', opinion: 'Puolesta'},
                { comment: 'Kyllä!', opinion: 'Puolesta'},
                { comment: 'Myönteinen vastaus!', opinion: 'Puolesta'},
                { comment: 'Tottakai!', opinion: 'Puolesta'},
                { comment: 'Ehdottomasti!', opinion: 'Puolesta'},
                { comment: 'Todellakin!', opinion: 'Puolesta'},
                { comment: 'Kyllä!', opinion: 'Puolesta'},
                { comment: 'Myönteinen vastaus!', opinion: 'Puolesta'},
                { comment: 'Tottakai!', opinion: 'Puolesta'},
            ]
        },
        {
            kategoria: 'Mäntsälän kunta',
            otsikko: 'Tekonurmen katsomot',
            kuvaus: 'Koen että olisi aika saada tekonurmelle hyvä paikka katsoa jalkapallo-ottelua. Onko muut miettineet tälläistä?',
            puolesta_aanet: 10,
            vastaan_aanet: 2,
            is_voted: false,
            id: 1,
            comments: [
                { comment: 'Todellakin!', opinion: 'puolesta' },
                { comment: 'Tää oli ihan paras viikonloppu!', opinion: 'puolesta' },
            ]
        },
        {
            kategoria: 'Riihenmäki',
            otsikko: 'Liikennevalot',
            kuvaus: 'Hirveää kyytiä taas autoja menee koulun lähellä. Mielestäni liikennevalot ratkaisisivat tämän ongelman jo osakseen. Ajatuksia?',
            puolesta_aanet: 20,
            vastaan_aanet: 3,
            is_voted: false,
            id: 2,
            comments: [],

        },
        {
            kategoria: 'Hirvihaara',
            otsikko: 'Tien kunto',
            kuvaus: 'Hirvihaaran läpi vievä tie on huonossa kunnossa. Tähän pitäisi saada muutos!',
            puolesta_aanet: 7,
            vastaan_aanet: 2,
            is_voted: false,
            id: 3,
            comments: [],
        },
        {
            kategoria: 'Mäntsälän kunta',
            otsikko: 'Uimahalli?',
            kuvaus: 'Missä se kauan odotettu uimahalli oikein viipyy? Olisi paljon potentiaalisia asiakkaita!',
            puolesta_aanet: 17,
            vastaan_aanet: 12,
            is_voted: false,
            id: 4,
            comments: [

                { comment: 'Minä ainakin kävisin', opinion: 'Puolesta' },
                { comment: 'Uikaa vaikka järvessä.', opinion: 'Vastaan' },
                { comment: 'Järvi on aika kaukana', opinion: 'Puolesta' },
                { comment: 'Rahalle parempaakin käyttöä.', opinion: 'Vastaan' },
                { comment: 'Kyllä pitäisi täälläkin olla uimahalli.', opinion: 'Puolesta' },
            ],

        },
        {
            kategoria: 'Mäntsälän lukio',
            otsikko: 'Kioskin valikoima',
            kuvaus: 'Pitäisikö kioskin valikoimaa laajentaa? Jos pitäisi niin mitä toivoisit tarjolle?',
            puolesta_aanet: 12,
            vastaan_aanet: 0,
            is_voted: false,
            id: 5,
            comments: [
                { comment: 'Jos saisi kunnollisia pullia.', opinion: 'Puolesta' },
                { comment: 'Aika vähän asiakkuutta on nyt mutta varmaan olisi enemmän jos olisi enemmän vaihtoehtoja.', opinion: 'Puolesta' },
                { comment: 'Parantakaa myös olemassaolevia tuotteita. Kahvi oli viime kerralla kylmää.', opinion: 'Puolesta' },
                { comment: 'Juomavalikoima on aika pieni.', opinion: 'Puolesta' },
                { comment: 'Olisi kiva jos olisi vähän raskaampaa syötävää myös.', opinion: 'Puolesta' },
                { comment: 'Enemmän limsoja.', opinion: 'Puolesta' },
            ],

        },
        {
            kategoria: 'Lavutie',
            otsikko: 'Kuusiaita',
            kuvaus: 'Aattelin kuusiaidan rakentaa tohon talon eteen. Koetko että haittaa katukuvaa?',
            puolesta_aanet: 1,
            vastaan_aanet: 25,
            is_voted: false,
            id: 6,
            comments: [
                { comment: 'Näyttäis todella oudolta jos semmoisen laittaisin.', opinion: 'Vastaan' },
                { comment: 'Ylempi sanoi hyvin.', opinion: 'Vastaan' },
                { comment: 'Tuli jotenkin klaustrofobinen olo kävellä ohi jos siihen laittaisit.', opinion: 'Vastaan' },
                { comment: 'Ihme hienostelua.', opinion: 'Vastaan' },
                { comment: 'Todellakin haittaa.', opinion: 'Vastaan' },
                { comment: 'Ihme puunvihaajia katu täynnä.', opinion: 'Puolesta' },
            ],

        }
    ],
    channels: [
        'Mäntsälän kunta',
        'Hirvihaara',
    ]

};


const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'VOTE': {
            console.log(action.payload)
            let initiatives = cloneDeep(state.initiatives);
            for (let i = 0; i < initiatives.length; i++) {
                let init = initiatives[i]
                if (init.id === action.payload.id) {
                    console.log(init, action.payload)
                    initiatives[i] = action.payload;
                }
            }
            console.log(initiatives)
            return { ...state, initiatives: initiatives }
        }

        case 'JOIN_CHANNEL': {
            let channels = cloneDeep(state.channels);
            channels.push(action.payload);
            return loop(
                { ...state, channels: channels },
                Run(redirect)
            )
        }

        case 'CREATE_INITIATIVE': {
            let initiatives = cloneDeep(state.initiatives);
            let payload = cloneDeep(action.payload);
            payload.id = initiatives.length;
            initiatives.unshift(payload);
            return loop(
                { ...state, initiatives: initiatives },
                Run(redirect),
            )
        }

        default:
            return { ...state };
    }
}

export default reducer;