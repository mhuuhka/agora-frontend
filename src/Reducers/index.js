import {combineReducers} from 'redux-loop';
import reducer from './reducer';

const rootReducer = combineReducers({
    reducer: reducer,
});

export default rootReducer;
