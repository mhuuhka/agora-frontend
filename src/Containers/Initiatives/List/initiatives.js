import React, { Component } from 'react';
import { connect } from 'react-redux';
import InitiativeCard from '../../../Components/InitiativeCard/initiativeCard';
import './styles.css'
import { vote } from '../../../ActionCreators/user';
function mapStateToProps(state) {
    return {
        store: state.reducer
    };
}

function mapDispatchToProps(dispatch) {
    return {
        vote: (initiative) => dispatch(vote(initiative))
    }
}

class ListInitiatives extends Component {

    vote = (initiative) => {
        this.props.vote(initiative)
    }
    render() {
        console.log(this.props.store.initiatives)
        let initiatives = this.props.store.initiatives.map((initiative) => {
            if (this.props.store.channels.includes(initiative.kategoria) && !initiative.hidden) {
                return <InitiativeCard data={initiative} vote={this.vote} key={initiative.id + 'init'} />
            }
        })
        return (
            <div className="initiatives-wrapper">
                {initiatives}
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListInitiatives);