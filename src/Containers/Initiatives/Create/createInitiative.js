import React, { Component } from 'react';
import { connect } from 'react-redux';
import './styles.css';
import { createInitiative } from '../../../ActionCreators/user';
import cloneDeep from 'lodash/cloneDeep';
import { EEXIST } from 'constants';

function mapStateToProps(state) {
    return {
        store: state.reducer,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        createInitiative: (initiative) => dispatch(createInitiative(initiative))
    };
}

class CreateInitiative extends Component {
    state = {
            kategoria: '',
            otsikko: '',
            kuvaus: '',
            puolesta_aanet: 0,
            vastaan_aanet: 0,
            is_voted: false,
            hidden: false,
            comments: [],
    }

    onChange = (e) => {
        let state = cloneDeep(this.state)
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    onSelect = (e) => {
        let state = cloneDeep(this.state)
        state.kategoria = e.target.value;
        this.setState(state);
    }

    onSelectOpinion = (e) => {
        let state = cloneDeep(this.state)
        if(e.target.value === 'puolesta'){
            state.puolesta_aanet = 1;
            state.vastaan_aanet = 0;
        } else {
            state.puolesta_aanet = 0;
            state.vastaan_aanet = 1;
        }
        state.is_voted = true;
        this.setState(state)
    }

    render() {
        let channels = this.props.store.channels.map((channel) => (<option>{channel}</option>))

        return (
            <div>
                <h3 className="create-heading">Luo aloite</h3>
                <div className="inputs-container">
                    <div className="heading-input-container">
                        <input type="text" className="heading-input" name="otsikko" placeholder="Otsikko" onChange={this.onChange}/>
                    </div>

                    <div className="category-dropdown-container">
                        <select className="category-dropdown" placeholder="valitse yhteisö" name="kategoria" onChange={this.onSelect}>
                            <option hidden disabled selected>Valitse yhteisö</option>
                            {channels}
                        </select>
                    </div>

                    <div className="opinion-dropdown-container">
                        <select className="opinion-dropdown" onChange={this.onSelectOpinion}>
                            <option>Valitse kanta</option>
                            <option value="puolesta">Asian puolesta</option>
                            <option value="vastaan">Asiaa vastaan</option>
                        </select>
                    </div>
                    <div className="text-area-container">
                        <textarea className="description-input" rows="6" placeholder="Kuvaile aloitettasi tähän" name="kuvaus" onChange={this.onChange} />
                    </div>
                </div>

                <button className="btn btn-primary submit-button" onClick={e => this.props.createInitiative(this.state)}>Luo aloite</button>
            </div>
        );
    }
}

export default connect(
    mapStateToProps, mapDispatchToProps
)(CreateInitiative);