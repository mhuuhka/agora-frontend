import React, { Component } from 'react';
import { connect } from 'react-redux';

function mapStateToProps(state) {
    return {
        store: state.reducer
    };
}

function mapDispatchToProps(dispatch) {
    return {

    };
}


const Comment = (props) => {
    console.log(props)
    return <div style={{ marginBottom: '10px', border: 'solid 1px #dddddd', padding: '2px', backgroundColor: 'white', borderRadius: '5px', }}>
        <div style={{ display: 'inline-block', width: '100%', verticalAlign: 'top', padding: '5px', paddingTop: '0px' }}>
            <small style={props.data.opinion === 'Puolesta' || props.data.opinion === 'puolesta' ? { display: 'inline-block', width: '100%', color: 'green' } : { display: 'inline-block', width: '100%', color: 'firebrick' }}>{props.data.opinion}</small>
            <p style={{ width: '100%' }}>{props.data.comment}</p>
        </div>
        <div style={{ display: 'inline-block', width: '60%', height: '100%' }}>
            <div style={{ display: 'inline-block', width: '100px', height: '33%', padding: '10px', color: 'blue' }}>
                Tykkää
            </div>
        </div>
        <div style={{ display: 'inline-block', width: '40%', height: '100%' }}>
            <div style={{ display: 'inline-block', width: '100%', height: '33%', paddingRight: '10px', color: 'tomato', textAlign: 'right' }}>
                <i className="fa fa-flag"></i>
            </div>
        </div>
    </div>
}



class Comments extends Component {

    render() {
        let rows = [];

        let id = this.props.location.pathname.replace('/comments/', '');
        let comments = this.props.store.initiatives[id].comments;

        for (let row of comments) {
            rows.push(<Comment data={row} />)
        }
        return (
            <div style={{ padding: '10px' }}>
                <div className="card" style={{ padding: '10px', marginBottom: '20px' }}>
                    <p>{this.props.store.initiatives[id].kategoria}</p>
                    <h3>{this.props.store.initiatives[id].otsikko}</h3>
                    <p>{this.props.store.initiatives[id].kuvaus}</p>
                    <a style={{ textAlign: 'right', color: 'blue' }}>Kommentoi</a>
                </div>


                <h5>Kommentit</h5>
                {rows}
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
)(Comments);