import React, { Component } from 'react';
import './styles.css';

class LandingPage extends Component {
    redirect = (e) => {
        window.location.href = '/userinfo/'
    }
    render() {
        return (
            <div>
                <img src={require('../../Images/logo.png')} alt="logo" width="400" className="logo" />
                <h3 style={{textAlign: 'center'}}>Osallistu ja vaikuta</h3>
                <div className="button-div" onClick={this.redirect}
                    style={{
                        height: '80px',
                        width: '80px',
                        backgroundPosition: 'center',
                        backgroundSize: 'cover',
                        backgroundRepeat: 'no-repeat',
                        backgroundImage: `url(${require('../../Images/arrow-right.png')})`,
                        margin: '0 auto',
                        marginTop: '70px',

                    }}></div>
            </div>
        );
    }
}

export default LandingPage;