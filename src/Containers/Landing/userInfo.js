import React, { Component } from 'react';
import './styles.css';
import { LIST_INITIATIVES_PATH } from '../../Constants/globals';

class UserInfo extends Component {
    redirect = (e) => {
        window.location.href = LIST_INITIATIVES_PATH;
    }
    render() {
        return (
            <div className="userinfo">
                <img src={require('../../Images/logo.png')} alt="logo" width="400" className="logo-small" />
                <div className="select-container">
                    <h3 className="heading-text">Valitse alue</h3>
                    <input className="area-input" placeholder="postinumero" />
                </div>
                <div className="button-div" onClick={this.redirect}
                    style={{
                        height: '60px',
                        width: '60px',
                        backgroundPosition: 'center',
                        backgroundSize: 'cover',
                        backgroundRepeat: 'no-repeat',
                        backgroundImage: `url(${require('../../Images/arrow-right.png')})`,
                        margin: '0 auto',
                        marginTop: '40px',

                    }}></div>

            </div>
        );
    }
}

export default UserInfo;