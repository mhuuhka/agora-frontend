import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { LIST_INITIATIVES_PATH, CREATE_INITIATIVE_PATH, LIST_CHANNELS_PATH } from '../../Constants/globals';
import ListInitiatives from '../Initiatives/List/initiatives';
import './wrappers.css'
import Header from '../../Components/Header/header';
import Footer from '../../Components/Footer/footer';
import CreateInitiative from '../Initiatives/Create/createInitiative';
import ListChannels from '../Channels/List/channels';
import LandingPage from '../Landing/landingPage';
import UserInfo from '../Landing/userInfo';
import Comments from '../Initiatives/Comments/comments';

class ContentWrapper extends Component {
    render() {
        let backend_url = 'http://demoagora.herokuapp.com/';
        console.log(backend_url)
        if (document.URL === backend_url || document.URL === backend_url + 'userinfo/') {
            return (<div className="content-wrapper">
                <Router>
                    <Switch>
                        <Route exact path={'/'} component={LandingPage} />
                        <Route exact path={'/userinfo'} component={UserInfo}/>
                    </Switch>
                </Router>
            </div>)
        } else {

        }
        return (
            <div className="content-wrapper">
                <Header />
                <div className="content-container">
                    <Router>
                        <Switch>
                            <Route exact path={LIST_INITIATIVES_PATH} component={ListInitiatives} />
                            <Route exact path={CREATE_INITIATIVE_PATH} component={CreateInitiative} />
                            <Route exact path={LIST_CHANNELS_PATH} component={ListChannels} />
                            <Route path={'/comments'} component={Comments} />
                        </Switch>
                    </Router>
                </div>
                <Footer />
            </div>
        );
    }
}

export default ContentWrapper;