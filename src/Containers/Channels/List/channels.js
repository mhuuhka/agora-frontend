import React, { Component } from 'react';
import { connect } from 'react-redux';
import './styles.css';
import { joinChannel } from '../../../ActionCreators/user';
function mapStateToProps(state) {
    return {
        store: state.reducer,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        joinChannel: (channel) => dispatch(joinChannel(channel))
    };
}

const ChannelRow = props => (<div className="channel-row">
    <a className="channel-name">{props.name}</a>
    <button className="btn btn-link" onClick={e => props.joinChannel(props.name)}>{props.join ? 'Liity' : ''}</button>
</div>)

class ListChannels extends Component {

    joinChannel = (channel) => {
        this.props.joinChannel(channel);
    }
    render() {
        let channels = ['Ting Hackathon'];
        let joinableChannels = [];
        for (let row of channels) {
            if (!this.props.store.channels.includes(row)) {
                joinableChannels.push(row);
            }
        }
        return (
            <div className="list-channels-container">
                <h3 className="channels-heading">Yhteisöt</h3>

                <small>Kunnalliset yhteisöt</small>
                <div className="channels-card">
                    <ChannelRow name="Mäntsälän kunta" />
                    <ChannelRow name="Hirvihaara" />
                    <ChannelRow name="Terveydenhuolto" />

                </div>

                <small>Omat yhteisöt</small>
                <div className="channels-card">
                    {this.props.store.channels.map((channel) => {
                        if (channel !== 'Mäntsälän kunta' && channel !== 'Terveyden huolto' && channel !== 'Hirvihaara') {
                            return <ChannelRow name={channel} />
                        }

                    })}
                </div>

                <small>Ehdotetut yhteisöt</small>
                <div className="channels-card">
                    {joinableChannels.map((channel) => {
                        return <ChannelRow name="Lavutie" join name={channel} joinChannel={this.joinChannel} />
                    })}
                    {joinableChannels.length === 0 ? 'Ei ehdotuksia' : ''}
                </div>

                <div className="searchbar-container">
                    <input type="text" className="searchbar fas" placeholder="&#xf002; Hae yhteisöä" />
                </div>
                <h5 style={{textAlign: 'center', paddingTop: '20px'}}>Tai</h5>
                <button className="btn btn-link" style={{margin: '0 auto', display: 'block', fontSize: '20px'}}>Luo uusi ryhmä</button>
            </div>
        );
    }
}

export default connect(
    mapStateToProps, mapDispatchToProps
)(ListChannels);